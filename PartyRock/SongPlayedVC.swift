//
//  SongPlayedVC.swift
//  PartyRock
//
//  Created by Mustafa Khalil on 2/5/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import UIKit

class SongPlayedVC: UIViewController {

    @IBOutlet weak var nameSongLabel: UILabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    private var _partyRock: DataModel!
    
    var partyRock: DataModel{
        set{
            _partyRock = newValue
        }
        get{
            return _partyRock
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameSongLabel.text = _partyRock.videoTitle
        webView.loadHTMLString(_partyRock.videoURL, baseURL: nil)

    }

    @IBAction func endButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }


}
