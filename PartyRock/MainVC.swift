//
//  ViewController.swift
//  PartyRock
//
//  Created by Mustafa Khalil on 2/5/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    

    var cellPartyRock = [DataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        let p1 = DataModel(imageUrl: "http://www.wavefm.com.au/images/stories/2015/04/redfoo.jpg", videoUrl: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ev4bY1SkZLg\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Lights Out")
        
        let p2 = DataModel(imageUrl: "http://www.croshalgroup.com/wp-content/uploads/2015/05/Redfoo-Website.jpg", videoUrl: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/1w9DiGlZksU\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Let's Get Ridiculous")
        let p3 = DataModel(imageUrl: "https://i.ytimg.com/vi/2wUxw6GH3IM/hqdefault.jpg", videoUrl: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/vg_nvEGryA4\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Juicy Wiggle Lesson")
        let p4 = DataModel(imageUrl: "http://www.billboard.com/files/styles/article_main_image/public/media/lmfao-party-rock-anthem-2011-billboard-650.jpg", videoUrl: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/gZIqW92GaTQ\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Party Rock Commercial")
        let p5 = DataModel(imageUrl: "http://direct-ns.rhap.com/imageserver/v2/albums/Alb.219913217/images/600x600.jpg", videoUrl: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tWyuglGCKgE\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Juicy Wiggle")

        cellPartyRock.append(p1)
        cellPartyRock.append(p2)
        cellPartyRock.append(p3)
        cellPartyRock.append(p4)
        cellPartyRock.append(p5)
 
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCell", for: indexPath) as? PartyCell{
            
            let partyRock = cellPartyRock[indexPath.row]
                
            cell.updateUI(PartyRock: partyRock)
            
            return cell
        }
        else{
            
          return UITableViewCell()
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellPartyRock.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let partyrock = cellPartyRock[indexPath.row]
        performSegue(withIdentifier: "SongPlayedSegue", sender: partyrock)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let path = segue.destination as? SongPlayedVC{
            if let partyrock = sender as? DataModel{
                path.partyRock = partyrock
            }
        }
    }

}

