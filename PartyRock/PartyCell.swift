//
//  PartyCell.swift
//  PartyRock
//
//  Created by Mustafa Khalil on 2/7/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import UIKit

class PartyCell: UITableViewCell {

    
    @IBOutlet weak var videoPreviewImage: UIImageView!
    
    @IBOutlet weak var videoName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func updateUI(PartyRock: DataModel) {
        videoName.text = PartyRock.videoTitle
        //videoPreviewImage
        let url = URL(string: PartyRock.imageURL)
        DispatchQueue.global().async {
            do{
                let data = try Data(contentsOf: url!)
                DispatchQueue.global().sync {
                self.videoPreviewImage.image = UIImage(data: data)
                }
            }
            catch{
                
            }
        }
    }


}
