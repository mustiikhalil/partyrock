//
//  DataModel.swift
//  PartyRock
//
//  Created by Mustafa Khalil on 2/7/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import Foundation


class DataModel {
    private var _imageURL: String!
    private var _videoURL: String!
    private var _videoTitle: String!
    
    var imageURL: String{
        return _imageURL
    }
    
    var videoURL: String{
        return _videoURL
    }
    
    var videoTitle: String{
        return _videoTitle
    }
    
    
    init(imageUrl: String, videoUrl: String, videoTitle: String) {
        self._imageURL = imageUrl
        self._videoURL = videoUrl
        self._videoTitle = videoTitle
    }
    
    
}
